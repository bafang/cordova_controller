var express = require('express');
var app = express();

var server_url = "192.168.254.36";
var server_port = 3001;
var io = require('socket.io').listen(3002);
var fs = require('fs');


app.use(express.urlencoded());
app.use('/assets', express.static('assets'));
app.get('/', function (req, res) {
	var path = req.params[0] ? req.params[0] : 'index.html';
   //res.sendFile(path, {root: './assets'});
   res.redirect('/assets/index.html');
});
app.post('/', function(req, res){
	//console.log('Post recieved at http://%s:%s', server_url, server_port);
	//print(request, response);
	//console.log(req.query);
	//console.log(req.params);
	//console.log(request.originalUrl);
	//console.log(req.body);
	console.log(req.body.tag+": "+req.body.message);
		io.sockets.emit('log', {
         tag: req.body.tag,
         message: req.body.message
      });
   res.send({
   	status_code: 1,
   	message: ""
   });	
});
var server = app.listen(server_port, function () {
	  var host = server.address().address;
	  var port = server.address().port;

	  console.log('Example app listening at http://%s:%s', host, port);
});

process.on('SIGINT', function() {    
    process.exit();
});
