// (c) 2014 Don Coleman
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/* global mainPage, deviceList, refreshButton */
/* global detailPage, batteryState, batteryStateButton, disconnectButton */
/* global ble  */
/* jshint browser: true , devel: true*/
'use strict';

var log = {
	url: "http://192.168.254.36:3001",
	debug: function(m){
		$.ajax({ method: "POST", url: log.url, data: {tag:"debug", message: m}, success: function(o){  }});
	},
	info: function(m){
		$.ajax({ method: "POST", url: log.url, data: {tag:"info", message: m}, success: function(o){  }});
	},
	error: function(m){
		$.ajax({ method: "POST", url: log.url, data: {tag:"error", message: m}, success: function(o){  }});
	}
};
/*

{"name":"ArduinoHM10","id":"20:C3:8F:F6:7B:48","advertising":{},"rssi":-52}

{
"name":"ArduinoHM10",
"id":"20:C3:8F:F6:7B:48",
"advertising":{},
"rssi":-52,
"services":["1800","1801","ffe0"],
"characteristics":[{
	"service":"1800",
	"characteristic":"2a00",
	"properties":["Read"]
	},{
	"service":"1800",
	"characteristic":"2a01",
	"properties":["Read"]
	},{
	"service":"1800",
	"characteristic":"2a02",
	"properties":["Read","Write"]
	},{
	"service":"1800",
	"characteristic":"2a03",
	"properties":["Read","Write"]
	},{
	"service":"1800",
	"characteristic":"2a04",
	"properties":["Read"]
	},{
	"service":"1801",
	"characteristic":"2a05",
	"properties":["Indicate"],
	"descriptors":[{"uuid":"2902"}]
	},{
	"service":"ffe0",
	"characteristic":"ffe1",
	"properties":["Read","WriteWithoutResponse","Notify"],
	"descriptors":[{"uuid":"2902"},{"uuid":"2901"}]
}]}


https://github.com/don/cordova-plugin-ble-central
*/
var app = {
	deviceID: "20:C3:8F:F6:7B:48",
	deviceName: "ArduinoHM10",
	UI: {
		deviceList: null,
		refreshButton: null,
		disconnectButton: null,
		mainPage: null,
		detailPage: null,
		deviceDetail: null
	},
    initialize: function() {        
		document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
		try{
			app.UI.deviceList = document.getElementById("deviceList");
			app.UI.refreshButton = document.getElementById("refreshButton");
			app.UI.disconnectButton = document.getElementById("disconnectButton");
			app.UI.mainPage = document.getElementById("mainPage");
			app.UI.detailPage = document.getElementById("detailPage");
			app.UI.deviceDetail = document.getElementById("deviceDetail");
	
			app.UI.refreshButton.addEventListener('touchstart', app.refreshDeviceList, false);
			app.UI.disconnectButton.addEventListener('touchstart', app.disconnect, false);
			app.UI.deviceList.addEventListener('touchstart', app.connect, false); // assume not scrolling
			app.UI.detailPage.hidden = true;
			app.refreshDeviceList();
		
			/*log.debug("debug_test");
			log.info("info_test");
			log.error("error_test");*/
		} catch(e){
			log.error(e);
		}
		//alert("Document Ready");		
    },
	enableBLE: function(callBack){
		ble.enable(function(){
			log.info("ble is enabled");
			if(typeof(callBack) == 'function'){
				callBack();
			}
		}, function(){
			log.info("ble is still disabled");
			//the following works on android but is dumb
			/*ble.showBluetoothSettings(function(){
				log.info("ble is enabled!");
			}, function(){
				log.info("ble is still disabled !<");
			});*/
			app.enableBLE(callBack);
		});
	},
	scanDevices: function(){
		log.info("starting scan ...");
		ble.scan([], 5, app.onDiscoverDevice, app.onError);
		/*
		ble.startScanWithOptions(
			[],	//tried "1800","1801","ffe0" but i think they have to be advertised
			{ reportDuplicates: false }, app.onDiscoverDevice, app.onError
		);
		setTimeout(app.stopScan, 5000);
		//all services search still didnt find it
		*/
	},
	stopScan: function(){
		log.info("stopping scan ...");
		ble.stopScan(app.onSuccess, app.onError);
	},
    refreshDeviceList: function() {
        app.UI.deviceList.innerHTML = ''; // empties the list
		try {
			ble.isEnabled(function(){
				// scan for all devices
				app.scanDevices();
			}, function(){
				log.info("ble is disabled");
				app.enableBLE(app.scanDevices);
			});
		} catch(e){
			log.error(e);
		}
    },	
    onDiscoverDevice: function(device) {
        var listItem = document.createElement('li'),
            html = '<b>' + device.name + '</b><br/>' +
                'RSSI: ' + device.rssi + '&nbsp;|&nbsp;' +
                device.id;

        listItem.dataset.deviceId = device.id;  // TODO
        listItem.innerHTML = html;
        app.UI.deviceList.appendChild(listItem);
		log.info(JSON.stringify(device));
		if(device.id == app.deviceID){
			log.info("HM-10 found");
			ble.stopScan(function(o){
				log.info("scan stopped");
				app.connectDevice(app.deviceID);
			}, app.onError);
			
		}
    },
	connectDevice: function(deviceId){
        ble.connect(deviceId, function(d) {
			//ble.startNotification(deviceId, battery.service, battery.level, app.onBatteryLevelChange, app.onError);
			try{
				app.UI.disconnectButton.dataset.deviceId = deviceId;
				log.info(JSON.stringify(d));
				app.showDetailPage(d);
				/*
					"service":"ffe0",
					"characteristic":"ffe1",
					"properties":["Read","WriteWithoutResponse","Notify"],
					"descriptors":[{"uuid":"2902"},{"uuid":"2901"}]
				*/
				ble.startNotification(app.deviceID, "ffe0", "ffe1", function(o){
					log.debug("notify: "+JSON.stringify(o));
				}, app.onError);
			} catch(e){
				log.error(e);
			}
        }, app.onError);	
	},
    connect: function(e) {
        var deviceId = e.target.dataset.deviceId;
		log.info("connect to device "+deviceId);
		app.connectDevice(deviceId);
    },
    disconnect: function(event) {
		log.info("disconnecting ...");
        //var deviceId = event.target.dataset.deviceId;
		ble.disconnect(app.deviceID, app.showMainPage, app.onError);
		
		//success never gets called, hangs...
		/*ble.stopNotifcation(app.deviceID, "ffe0", "ffe1", function(o){
			log.info("notify stopped");
			ble.disconnect(app.deviceID, app.showMainPage, app.onError);
		}, app.onError);
		*/
    },
    showMainPage: function() {
        app.UI.mainPage.hidden = false;
        app.UI.detailPage.hidden = true;
    },
    showDetailPage: function(d) {
        app.UI.mainPage.hidden = true;
        app.UI.detailPage.hidden = false;
		app.UI.deviceDetail.innerHTML = JSON.stringify(d);
    },
    onSuccess: function(o) {
		log.info(JSON.stringify(o));
    },	
    onError: function(o) {
		log.error("onError: "+JSON.stringify(o));
    }
};
