
function arrayToString(r, start, len){
	var s = new String();
	for(var i=start;i<(start+len);i++){
		s += (String.fromCharCode(r[i]));
	}
	return s;
}
function sub_Array(r, start, len){
	var a = new Array();
	for(var i=start;i<(start+len);i++){
		a.push(r[i]);
	}
	return a;
}
function getKey(o, b){
	for(key in o){
		if(o[key] == b)
			return key;
	}
}

function simpleCRC(a){
	var s = 0;
	for(var i=0;i<a.length-1;i++){
		s += a[i];
	}
	return s % 256;
}

function bafang_software(){
	var wheel_dia = {
		INCH_16  : 0x1F,
		INCH_16a : 0x20,
		INCH_17  : 0x21,
		INCH_17a : 0x22,
		INCH_18  : 0x23,
		INCH_18a : 0x24,
		INCH_19  : 0x25,
		INCH_19a : 0x26,
		INCH_20  : 0x27,
		INCH_20a : 0x28,
		INCH_21  : 0x29,
		INCH_21a : 0x2A,
		INCH_22  : 0x2B,
		INCH_22a : 0x2C,
		INCH_23  : 0x2D,
		INCH_23a : 0x2E,
		INCH_24  : 0x2F,
		INCH_24a : 0x30,
		INCH_25  : 0x31,
		INCH_25a : 0x32,
		INCH_26  : 0x33,
		INCH_26a : 0x34,
		INCH_27 :  0x35,
		INCH_27a : 0x36,
		SIZE_700C : 0x37,
		INCH_28  : 0x38,
		INCH_28a : 0x39,
		INCH_29  : 0x3A,
		INCH_29a : 0x3B,
		INCH_30  : 0x3C,
		INCH_30a : 0x3D	
	};
	var speed_meter_model = {
		EXTERNAL_WHEEL_METER: 1,
		INTERNAL_MOTOR_METER: 65,
		BY_MOTOR_PHASE: 193
	};
	var pedal_type = {
		DoubleSignal24: 3,
		BBSensor32: 2,
		DHSensor12: 1,
		None: 255
	};
	var slow_start_mode = {
		1: 1,
		2: 2,
		3: 3,
		4: 4,
		5: 5,
		6: 6,
		7: 7,
		8: 8	
	};
	var work_mode = {
		Undeterminated: 255,
		10: 10,
		11: 11,
		12: 12,
		13: 13,
		14: 14,	
		15: 15,
		16: 16,
		17: 17,
		18: 18,
		19: 19,
		20: 20,
		21: 21,
		22: 22,
		23: 23,
		24: 24,
		25: 25,
		26: 26,
		27: 27,
		28: 28,
		29: 29,
		30: 30,
		31: 31,
		32: 32,		
		33: 33,
		34: 34,
		35: 35,
		36: 36,
		37: 37,
		38: 38,
		39: 39,
		40: 40,
		41: 41,
		42: 42,
		43: 43,
		44: 44,
		45: 45,	
		46: 46,
		47: 47,
		48: 48,
		49: 49,
		50: 50,
		51: 51,
		52: 52,
		53: 53,
		54: 54,
		55: 55,	
		56: 56,
		57: 57,
		58: 58,
		59: 59,
		60: 60,
		61: 61,
		62: 62,
		63: 63,
		64: 64,
		65: 65,	
		66: 66,
		67: 67,
		68: 68,
		69: 69,
		70: 70,
		71: 71,
		72: 72,
		73: 73,
		74: 74,
		75: 75,	
		76: 76,
		77: 77,
		78: 78,
		79: 79,
		80: 80		
	};
	var throttle_mode = {
		Speed: 255,
		Current: 1
	};
	var designated_assist = {
		ByDisplaysCommand: 255,
		0: 255,
		1: 1,
		2: 2,
		3: 3,
		4: 4,
		5: 5,
		6: 6,
		7: 7,
		8: 8,
		9: 9	
	};
	var speed_limited = {
		ByDisplaysCommand: 255,
		15: 15,
		16: 16,
		17: 17,
		18: 18,
		19: 19,
		20: 20,
		21: 21,
		22: 22,
		23: 23,
		24: 24,
		25: 25,
		26: 26,
		27: 27,
		28: 28,
		29: 29,
		30: 30,
		31: 31,
		32: 32,		
		33: 33,
		34: 34,
		35: 35,
		36: 36,
		37: 37,
		38: 38,
		39: 39,
		40: 40	
	};	
	function parse_info(response){
		function getVoltage(b){
			if(b == 0)
				return 24;
			if(b == 1)
				return 36;
			if(b == 2)
				return 48;
			if(b == 3)
				return 60;
		//        VOLTAGE_24_48 = 0x04,
		//        VOLTAGE_24_60 = Pass		
		}

						  
		if (response.length != 19)
			return false;
		if(simpleCRC(response) == response[response.length-1]){
			var info = {
				command: response[0],
				status: response[1],
				manufacturer: arrayToString(response, 2, 4),
				model: arrayToString(response, 6, 4),
				HWVersion: arrayToString(response, 10, 2),
				FWVersion: arrayToString(response, 12, 4),
				Voltage: getVoltage(response[16]), 
				max_current: response[17],
				crc: response[18]
			};	
			
			info.HWVersion = info.HWVersion[0]+'.'+info.HWVersion[1];
			info.FWVersion = info.FWVersion[0]+'.'+info.FWVersion[1]+'.'+info.FWVersion[2]+'.'+info.FWVersion[3];
			console.log(info);
			return info;
		}
		return false;
	}

	function parse_page1(response){
		if (response.length != 27)
			return false;
		if(simpleCRC(response) == response[response.length-1]){
			var page1 = {
				max_current: response[1],
				low_voltage: response[2],
				asst_currents: sub_Array(response, 3, 10),
				asst_speeds: sub_Array(response, 13, 10),
				speed_sig: response[23],
				wheel_dia: getKey(wheel_dia, response[24]),
				spd_meter_model: getKey(speed_meter_model, response[25]),
				crc: response[26]
			};
			console.log(page1);
			return page1;
		}
		return false;
	}

	function parse_page2(response){
		/*
		[83,11,3,255,255,40,5,4,255,25,8,10,60,246]
		double_signal-24
		dispCMD
		dispCMD
		40
		5
		4
		undetermined
		25
		8
		10
		60*/
		if (response.length != 14)
			return false;
		if(simpleCRC(response) == response[response.length-1]){		
			var page2 = {
				pedal_type: getKey(pedal_type, response[2]),
				designated_assist: getKey(designated_assist, response[3]),
				speed_limited: getKey(speed_limited, response[4]),
				start_current: response[5],
				slow_start_mode: response[6],
				startup_degree: response[7],
				work_mode: getKey(work_mode, response[8]),
				stop_time: response[9],
				current_decay: response[10],
				stop_decay: response[11],
				keep_current: response[12],
				crc: response[13]
			};
			console.log(page2);
			return page2;
		}
		return false;
	}

	
	function parse_page3(response){
		/*[84,6,11,35,1,255,40,10,186]
		11
		35
		current
		dispCMD
		40km/h
		10
		*/
		if (response.length != 9)
			return false;
		if(simpleCRC(response) == response[response.length-1]){
			var page3 = {
				start_voltage: response[2],
				end_voltage: response[3],
				throttle_mode: getKey(throttle_mode, response[4]),
				designated_assist: getKey(designated_assist, response[5]),
				speed_limited: getKey(speed_limited, response[6]),
				start_current: response[7],
				crc: response[8]
			};
			console.log(page3);
			return page3;
		}
		return false;
	}
	
	this.read_info = function(cb){
		//var command = [17,81,04,176,05];
		//var response = [81,16,72,90,88,84,83,90,90,57,49,49,50,48,49,49,02,25,48]; //len=19	
		//return parse_info([81,16,72,90,88,84,83,90,90,57,49,49,50,48,49,49,02,25,48]);
						
		bt.write_read([17,81,04,176,05], 19, function(data){
			//log.info(data);
			cb(parse_info(data));
		});
	}
	
	this.read_page1 = function(){
		//var command_r = [17,82]; //read command 82
		//var command_w = [22,82,24,41,22,52,58,64,70,76,82,88,94,100,100,44,52,60,68,76,90,92,100,100,100,52,1,252	]; //rx 	82,24,106 success
		                   //22,82,24,41,22,33,20,30,40,50,60,70,80,90,100,10,20,30,40,50,60,70,80,90,100,52,1,65" //external meter
						   //22,82,24,41,22,33,20,30,40,50,60,70,80,90,100,10,20,30,40,50,60,70,80,90,100,52,65,129 //internal meter
						   //22,82,24,41,22,33,20,30,40,50,60,70,80,90,100,10,20,30,40,50,60,70,80,90,100,52,193,1 //motor phase
						   //speed sig gets added to speed model
		//return parse_page1([82,24,41,22,52,58,64,70,76,82,88,94,100,100,44,52,60,68,76,90,92,100,100,100,52,1,252]); //len = 27
		
		bt.write_read([17,82], 27, function(data){
			//log.info(data);
			cb(parse_page1(data));
		});
	}
	
	this.read_page2 = function(){
		//var command_r = [17,83];
		//var command_w = [22,83,11,3,255,255,40,5,4,255,25,8,10,60,246]; //rx 	83,11,94 success
						   //22,83,11,2,255,255,40,5,4,255,25,8,10,60,245 bbsens32
						   //22,83,11,1,255,255,40,5,4,255,25,8,10,60,244 dh-12
						   //22,83,11,255,255,40,5,4,255,25,8,10,60,243 none
						   //22,83,11,3,255,40,5,4,255,25,8,10,60,247 asst 0
						   //22,83,11,3,1,255,40,5,4,255,25,8,10,60,248 asst 1
						   //22,83,11,3,255,15,40,5,4,255,25,8,10,60,6 15kph
						   //22,83,11,3,255,255,40,5,4,255,25,8,10,60,246 disp cmd 4 speed
						   //22,83,11,3,255,255,40,5,4,10,25,8,10,60,1 wm=10
		//return parse_page2([83,11,3,255,255,40,5,4,255,25,8,10,60,246]);
		bt.write_read([17,83], 14, function(data){
			//log.info(data);
			cb(parse_page2(data));
		});		
	}
	
	this.read_page3 = function(){
		//var comand_r = [17,84];
		//var command_w = [22,84,6,11,35,1,255,40,10,186]; //rx 	84,6,90 success	
						   //22,84,6,11,35,1,255,255,10,145 current
						   //22,84,6,11,35,255,255,10,144 speed
						   //22,84,6,11,35,1,255,10,146 asst 0  missing byte!!!
						   //22,84,6,11,35,1,1,255,10,147 asst 1
		//return parse_page3([84,6,11,35,1,255,40,10,186]);
		bt.write_read([17,84], 9, function(data){
			//log.info(data);
			cb(parse_page3(data));
		});		
	}
}

var bafang = new bafang_software();


function read_speed(response){
	//var command = [22, 26, 240, 17, 32]; //tx 00, 00, 20
	if (response.length != 3)
		return null;	
}
/*	
byte _connect[] = {0x7E, 0xFE, 0x7E, 0xFE, 0x11, 0x08}; //[126, 254, 126, 254, 17, 8] //tx: 1

byte _read_speed[] = {0x16, 0x1A, 0xF0, 0x11, 0x20}; //[22, 26, 240, 17, 32]  //tx 00, 00, 20
byte _read_power[] = {0x11, 0x0A};      //[17, 10]  // tx 02, 02 
byte _ack[] = {0x11, 0x08}; 			//[17, 08]  //tx: 1
byte _ack1[] = {0x11, 0x11}; 			//[17, 17]  tx: 64, 64
byte _read1[] = {0x16, 0x0B, 0x0B, 0x2C, 0x11, 0x08}; //tx: 1
byte _read2[] = {0x16, 0x1F, 0x00, 0xE8, 0x1D, 0x16, 0x0B, 0x0B, 0x2C, 0x11, 0x08}; //tx: 1	


Common display error codes
No  code  underlying fault  No  code  underlying fault
6 09  Phase short circuit or open circuit 12  30  Communication abnormality of controller and display
1 04  Throttle not returning to zero state  7 10  Controller overheat
2 05  Throttle hallsensor abnormality 8 11  Abnormality of temperature sensor in controller
3 06  Low voltage protection  9 12  Current sensor abnormality
4 07  Over voltage protection 10  21  Speed sensor abnormality
5 08  Hallsensor abnormality or open circuit  11  22  Communication abnormality in BMS
6 09  Phase short circuit or open circuit
7  10  Controller overheat
8  11  Abnormality of temperature sensor in controller
9  12  Current sensor abnormality
10  21  Speed sensor abnormality
11  22  Communication abnormality in BMS
12  30  Communication abnormality of controller and display*/