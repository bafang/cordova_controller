// (c) 2014 Don Coleman
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/* global mainPage, deviceList, refreshButton */
/* global detailPage, batteryState, batteryStateButton, disconnectButton */
/* global ble  */
/* jshint browser: true , devel: true*/
'use strict';

var log = {
	url: "http://192.168.1.110:3001",
	debug: function(m){
		if(typeof(m) == 'object'){	m = JSON.stringify(m);	}
		$.ajax({ method: "POST", url: log.url, data: {tag:"debug", message: m}, success: function(o){  }});
		log_messages({tag:"debug", message: m});
	},
	info: function(m){
		if(typeof(m) == 'object'){	m = JSON.stringify(m);	}	
		$.ajax({ method: "POST", url: log.url, data: {tag:"info", message: m}, success: function(o){  }});
		log_messages({tag:"info", message: m});
	},
	error: function(m){
		if(typeof(m) == 'object'){	m = JSON.stringify(m);	}
		$.ajax({ method: "POST", url: log.url, data: {tag:"error", message: m}, success: function(o){  }});
		log_messages({tag:"error", message: m});
	}
};



var app = {
    initialize: function() {        
		document.addEventListener('deviceready', this.onDeviceReady, false);
		document.addEventListener("pause", this.onPause, false);
		document.addEventListener("resume", this.onResume, false);		
    },
    onDeviceReady: function() {
		try{
			init();
		} catch(e){
			log.error(e);
		}
		//alert("Document Ready");		
    },
	onPause: function() {
		// Handle the pause event
		try{
			onPause();
		} catch(e){
			log.error(e);
		}		
	},
	onResume: function() {
		// Handle the resume event
		try{
			onResume();
		} catch(e){
			log.error(e);
		}		
	},	
    onSuccess: function(o) {
		log.info(JSON.stringify(o));
    },	
    onError: function(o) {
		log.error("onError: "+JSON.stringify(o));
    }
};
