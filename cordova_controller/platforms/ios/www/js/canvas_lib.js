//requires easljs

function seven_seg(){
	/*
	 7-Segment-Anzeige

	  00000
	 5     1
	 5     1
	 5     1
	  66666
	 4     2
	 4     2
	 4     2
	  33333

	 Positionierung der einzelnen Segmente (innerhalb einer Ziffer):
	 Segment 0 = (x, y)
	 Segment 1 = (x+2*objSegment.height+objSegment.width+objSegment.dist, y+objSegment.dist)
	 Segment 2 = (x+2*objSegment.height+objSegment.width+objSegment.dist, y+2*objSegment.height+objSegment.width+3*objSegment.dist)
	 Segment 3 = (x, y+4*objSegment.height+2*objSegment.width+4*objSegment.dist)
	 Segment 4 = (x-objSegment.dist, y+2*objSegment.height+objSegment.width+3*objSegment.dist)
	 Segment 5 = (x-objSegment.dist, y+objSegment.dist)
	 Segment 6 = (x, y+2*objSegment.height+objSegment.width+2*objSegment.dist)
	*/


	//http://canvas.quaese.de/index.php?nav=13,75&doc_id=71
	var seven_seg_color = "000000";
	// Objekt zum Einstellen der Segmentanzeige
	var objSegment = {x: 10,     // x-Offset der ersten Ziffer im Canvas-Element
					  y: 14,     // y-Offset der ersten Ziffer im Canvas-Element
					  height: 2, // Entspricht der halben H�he
					  width: 7, // Entspricht der L�nge (Gesamtl�nge = width + 2*height)
					  dist: 1};  // Abstand zwischen den einzelnen Segmenten

	var shape = null;
	function drawHorizontalSegment(intOffX, intOffY){
	  /*ctx.beginPath();
	  ctx.moveTo(intOffX, intOffY);
	  ctx.lineTo(intOffX, intOffY);
	  ctx.lineTo(intOffX+objSegment.height, intOffY-objSegment.height);
	  ctx.lineTo(intOffX+objSegment.height+objSegment.width, intOffY-objSegment.height);
	  ctx.lineTo(intOffX+2*objSegment.height+objSegment.width, intOffY);
	  ctx.lineTo(intOffX+objSegment.height+objSegment.width, intOffY+objSegment.height);
	  ctx.lineTo(intOffX+objSegment.height, intOffY+objSegment.height);
	  ctx.lineTo(intOffX, intOffY);
	  drawSegment();*/
	  
	  //shape.graphics.beginPath();
	  shape.graphics.moveTo(intOffX, intOffY);
	  shape.graphics.lineTo(intOffX+objSegment.height, intOffY-objSegment.height);
	  shape.graphics.lineTo(intOffX+objSegment.height+objSegment.width, intOffY-objSegment.height);
	  shape.graphics.lineTo(intOffX+2*objSegment.height+objSegment.width, intOffY);
	  shape.graphics.lineTo(intOffX+objSegment.height+objSegment.width, intOffY+objSegment.height);
	  shape.graphics.lineTo(intOffX+objSegment.height, intOffY+objSegment.height);
	  shape.graphics.lineTo(intOffX, intOffY);
	  shape.graphics.closePath();		  
	}

	 
	function drawVerticalSegment(intOffX, intOffY){
	  /*ctx.beginPath();
	  ctx.moveTo(intOffX, intOffY);
	  ctx.lineTo(intOffX, intOffY);
	  ctx.lineTo(intOffX+objSegment.height, intOffY+objSegment.height);
	  ctx.lineTo(intOffX+objSegment.height, intOffY+objSegment.height+objSegment.width);
	  ctx.lineTo(intOffX, intOffY+2*objSegment.height+objSegment.width);
	  ctx.lineTo(intOffX-objSegment.height, intOffY+objSegment.height+objSegment.width);
	  ctx.lineTo(intOffX-objSegment.height, intOffY+objSegment.height);
	  ctx.lineTo(intOffX, intOffY);
	  drawSegment();*/
	  
	  shape.graphics.moveTo(intOffX, intOffY);
	  shape.graphics.lineTo(intOffX, intOffY);
	  shape.graphics.lineTo(intOffX+objSegment.height, intOffY+objSegment.height);
	  shape.graphics.lineTo(intOffX+objSegment.height, intOffY+objSegment.height+objSegment.width);
	  shape.graphics.lineTo(intOffX, intOffY+2*objSegment.height+objSegment.width);
	  shape.graphics.lineTo(intOffX-objSegment.height, intOffY+objSegment.height+objSegment.width);
	  shape.graphics.lineTo(intOffX-objSegment.height, intOffY+objSegment.height);
	  shape.graphics.lineTo(intOffX, intOffY);
		shape.graphics.closePath();		  
	}
	function drawSegment(){
	  ctx.save();
	  ctx.fillStyle = seven_seg_color; //"#fff";
	  ctx.fill();
	  ctx.restore();
	}
	
	function drawDigit(intDigit, x, y, strColor){
		// Falls keine g�ltige Ziffer �bergeben wurde
		if((intDigit<0) || (intDigit>9)) return;

	  // Boolwerte zu den jeweiligen Bits
	  var a = ((intDigit & 8) == 8);  // true, wenn 4-tes Bit gesetzt
	  var b = ((intDigit & 4) == 4);  // true, wenn 3-tes Bit gesetzt
	  var c = ((intDigit & 2) == 2);  // true, wenn 2-tes Bit gesetzt
	  var d = ((intDigit & 1) == 1);  // true, wenn 1-tes Bit gesetzt

	  /* Segmente zeichnen */
	  // Segment 0
	  if(a || c || (b&&d) || (!b&&!d))
		  drawHorizontalSegment(x, y);
	  // Segment 1
	  if(a || (!c&&!d) || !b || (c&&d))
		  drawVerticalSegment(x+2*objSegment.height+objSegment.width+objSegment.dist, y+objSegment.dist, strColor);
	  // Segment 2
	  if(!c || b || d)
		  drawVerticalSegment(x+2*objSegment.height+objSegment.width+objSegment.dist, y+2*objSegment.height+objSegment.width+3*objSegment.dist);
	  // Segment 3
	  if(a || (!b&&!c&&!d) || (!d&&c) || (!a&&!b&&c) || (b&&!c&&d))
		  drawHorizontalSegment(x, y+4*objSegment.height+2*objSegment.width+4*objSegment.dist);
	  // Segment 4
	  if((!b&&!d) || (!a&&c&&!d))
		  drawVerticalSegment(x-objSegment.dist, y+2*objSegment.height+objSegment.width+3*objSegment.dist);
	  // Segment 5
	  if((b&&!d) || (!c&&!d) || (b&&!c) || (a&&!c))
		  drawVerticalSegment(x-objSegment.dist, y+objSegment.dist);
	  // Segment 6
	  if(a || (c&&!d) || (!b&&c) || (b&&!c))
		  drawHorizontalSegment(x, y+2*objSegment.height+objSegment.width+2*objSegment.dist);
	}
	
	this.draw = function(num){
		shape = new createjs.Shape();
		//console.log(shape.graphics);
		var s = num.toString();
		shape.graphics.beginFill(seven_seg_color);
		drawDigit(s[0]/1, objSegment.dist*3, objSegment.dist*2);
		if(s.length >= 2)
			drawDigit(s[1]/1, objSegment.dist*3+20, objSegment.dist*2);
		if(s.length >= 3)
			drawDigit(s[2]/1, objSegment.dist*3+40, objSegment.dist*2);
		if(s.length >= 4)
			drawDigit(s[3]/1, objSegment.dist*3+60, objSegment.dist*2);
		shape.graphics.endFill(seven_seg_color);			
		return shape;
	}
}


function batt(){
	this.draw = function(perc){
		shape = new createjs.Shape();
		var w = 100;
		var h = 30;
		var sw = 2;
		var num_bars = 12;
		var bar_width = ((w - (sw*2) - sw*num_bars) / num_bars);
		var bar_height = h - (sw*2);
		var stroke_color = "#333333";
		var fill_color = "#333333";
		var box_color = "#808080";
		shape.graphics.setStrokeStyle(sw,"round");
		shape.graphics.beginStroke(stroke_color);
		shape.graphics.drawRect(sw/2, sw/2, w, h);
		shape.graphics.endStroke(stroke_color);
		
		shape.graphics.beginFill(fill_color);
		shape.graphics.drawRect(w, 8, 4, h*.5);
		shape.graphics.endFill(fill_color);
		
		shape.graphics.beginFill(box_color);
		shape.graphics.drawRect(3, 3, (w-4)*perc, bar_height);
		/*for(var i=0;i<num_bars;i++){
			if(perc >= ((i+1)/num_bars))
				shape.graphics.drawRect((sw+1)+i*(bar_width+2), sw+1, bar_width, bar_height);
		}*/
		shape.graphics.endFill(box_color);
		return shape;
	}
}

function arrow(){
	this.draw = function(perc){
		shape = new createjs.Shape();
		var w = 25;
		var h = 15;
		var sw = 2;
		var num_bars = 6;
		var stroke_color = "#000000";
		var fill_color = "#000000";
		shape.graphics.setStrokeStyle(sw,"round");
		//shape.graphics.beginStroke(stroke_color);
		//shape.graphics.endStroke(stroke_color);
		
		shape.graphics.beginFill(fill_color);
	  shape.graphics.moveTo(0, 0);
	  shape.graphics.lineTo(w/2, h);
	  shape.graphics.lineTo(w, 0);
	  shape.graphics.lineTo(0, 0);
	  shape.graphics.closePath();
		shape.graphics.endFill(fill_color);
		return shape;
	}		
}